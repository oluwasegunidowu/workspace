variable region {
    default = "us-east-2"
}

variable instance_type {
default = "t2.micro"
}

variable ami {
    type           = map
    default        = {
        "ProjectA" = "ami-0103f211a154d64a6"
        "ProjectB" = "ami-0a695f0d95cefc163"
        "ProjectC" = "ami-0ca58e4cb9e83244e"
    }
}